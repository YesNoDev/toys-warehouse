// ToyService.java
package com.mazaitov.service;

import com.mazaitov.model.Toy;

import java.util.List;

public interface ToyService {
    List<Toy> getAllToys();

    List<Toy> searchToysByName(String name);

    List<Toy> searchToysByPrice(double minPrice, double maxPrice);

    void addToy(Toy toy);

    void deleteToy(int toyId);

    void loadToysFromCsv(String filePath);

    void saveToysToCsv(String filePath);

    // Add this method
    List<String> getAllCategories();
}
