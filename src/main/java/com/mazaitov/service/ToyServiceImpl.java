// ToyServiceImpl.java
package com.mazaitov.service;

import com.mazaitov.model.Toy;
import com.mazaitov.repository.ToyRepository;

import java.util.List;

public class ToyServiceImpl implements ToyService {
    private final ToyRepository toyRepository;

    public ToyServiceImpl(ToyRepository toyRepository) {
        this.toyRepository = toyRepository;
    }

    @Override
    public List<Toy> getAllToys() {
        return toyRepository.getAllToys();
    }

    @Override
    public List<Toy> searchToysByName(String name) {
        return toyRepository.searchToysByName(name);
    }

    @Override
    public List<Toy> searchToysByPrice(double minPrice, double maxPrice) {
        return toyRepository.searchToysByPrice(minPrice, maxPrice);
    }

    @Override
    public void addToy(Toy toy) {
        toyRepository.addToy(toy);
    }

    @Override
    public void deleteToy(int toyId) {
        toyRepository.deleteToy(toyId);
    }

    @Override
    public void loadToysFromCsv(String filePath) {
        toyRepository.loadToysFromCsv(filePath);
    }

    @Override
    public void saveToysToCsv(String filePath) {
        toyRepository.saveToysToCsv(filePath);
    }

    // Add this method
    @Override
    public List<String> getAllCategories() {
        // Implement logic to get all categories
        // For simplicity, let's assume categories are stored in the repository
        return toyRepository.getAllCategories();
    }
}
