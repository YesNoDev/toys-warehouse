package com.mazaitov;

import com.mazaitov.model.Toy;
import com.mazaitov.controller.ToyController;
import com.mazaitov.repository.ToyRepository;
import com.mazaitov.repository.ToyRepositoryImpl;
import com.mazaitov.service.ToyService;
import com.mazaitov.service.ToyServiceImpl;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isAdmin = false; // Flag to track if the user is an administrator
        String adminUsername = "admin"; // Hardcoded admin username (you can replace it with your admin username)
        String adminPassword = "admin123"; // Hardcoded admin password (you can replace it with your admin password)

        // Toy functionality menu
        ToyRepository toyRepository = new ToyRepositoryImpl();
        ToyService toyService = new ToyServiceImpl(toyRepository);

        // Load toys from CSV file
        String filePath = "toys.csv";
        toyService.loadToysFromCsv(filePath);

        ToyController toyController = new ToyController(toyService);

        while (true) {
            System.out.println("\nToy Warehouse Menu:");
            System.out.println("1. Display all toys");
            System.out.println("2. Search toys by name");
            System.out.println("3. Search toys by price range");
            System.out.println("4. To become Mr.Admin");
            System.out.println("0. Exit");

            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character

            // Handle menu choices
            switch (choice) {
                case 1:
                    toyController.displayAllToys();
                    break;
                case 2:
                    System.out.print("Enter the toy name: ");
                    String name = scanner.nextLine();
                    toyController.searchToysByName(name);
                    break;
                case 3:
                    System.out.print("Enter the minimum price: ");
                    double minPrice = scanner.nextDouble();
                    System.out.print("Enter the maximum price: ");
                    double maxPrice = scanner.nextDouble();
                    toyController.searchToysByPrice(minPrice, maxPrice);
                    break;
                case 4:
                    // Authorization menu for becoming Mr.Admin
                    System.out.println("Write your username: ");
                    String username = scanner.nextLine();
                    System.out.println("Write your password: ");
                    String password = scanner.nextLine();

                    if (username.equals(adminUsername) && password.equals(adminPassword)) {
                        isAdmin = true;
                        System.out.println("Now You are Admin bro! Please, manage your Staff, tell me what to do");
                        // Additional admin options
                        while (true) {
                            System.out.println("\nAdmin Options:");
                            System.out.println("1. Add new records (elements)");
                            System.out.println("2. Delete records (elements)");
                            System.out.println("3. Add new types (categories)");
                            System.out.println("0. Exit");

                            System.out.print("Enter your choice: ");
                            int adminChoice = scanner.nextInt();
                            scanner.nextLine(); // Consume the newline character

                            // Handle admin menu choices
                            switch (adminChoice) {
                                case 1:
                                    // Add new records (elements) functionality for Admin
                                    System.out.println("Enter details for the new toy:");
                                    System.out.print("Enter the toy name: ");
                                    String newName = scanner.nextLine();
                                    System.out.print("Enter the toy category: ");
                                    String newCategory = scanner.nextLine();
                                    System.out.print("Enter the toy price: ");
                                    double newPrice = scanner.nextDouble();

                                    // Generate a new unique ID for the toy
                                    int newId = toyService.getAllToys().size() + 1;

                                    // Create a new Toy object and add it to the repository
                                    toyService.getAllToys().add(new Toy(newId, newName, newCategory, newPrice));

                                    System.out.println("New toy added successfully!");
                                    break;
                                case 2:
                                    // Delete records (elements) functionality for Admin
                                    System.out.print("Enter the ID of the toy to delete: ");
                                    int toyIdToDelete = scanner.nextInt();
                                    boolean deleted = toyController.deleteToyById(toyIdToDelete);

                                    if (deleted) {
                                        System.out.println("Toy deleted successfully!");
                                    } else {
                                        System.out.println("No toy found with ID: " + toyIdToDelete);
                                    }
                                    break;
                                case 3:
                                    // Add new types (categories) functionality for Admin
                                    System.out.print("Enter the new toy category: ");
                                    String newCategoryToAdd = scanner.nextLine();

                                    // Check if the category already exists
                                    if (!toyService.getAllCategories().contains(newCategoryToAdd)) {
                                        toyService.getAllCategories().add(newCategoryToAdd);
                                        System.out.println("New category added successfully!");
                                    } else {
                                        System.out.println("Category already exists!");
                                    }
                                    break;
                                case 0:
                                    // Exit for Admin
                                    System.out.println("Exiting...");
                                    System.exit(0);
                                    break;
                                default:
                                    System.out.println("Invalid choice. Please try again.");
                                    break;
                            }
                        }
                    } else {
                        System.out.println("Invalid credentials. Exiting...");
                        System.exit(0);
                    }
                    break;
                case 0:
                    // Exit for User
                    System.out.println("Exiting...");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        }
    }
}
