package com.mazaitov.repository;

import com.mazaitov.model.Toy;

import java.util.List;

// Data Access Object (DAO)
public interface ToyRepository {
    List<Toy> getAllToys();

    Toy getToyById(int id);

    List<Toy> searchToysByName(String name);

    List<Toy> searchToysByPrice(double minPrice, double maxPrice);

    void addToy(Toy toy);

    void deleteToy(int toyId);

    List<String> getAllCategories();  // Assuming categories are represented as strings

    void loadToysFromCsv(String filePath);

    void saveToysToCsv(String filePath);
}
