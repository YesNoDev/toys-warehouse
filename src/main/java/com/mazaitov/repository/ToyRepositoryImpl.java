package com.mazaitov.repository;

import com.mazaitov.model.Toy;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ToyRepositoryImpl implements ToyRepository {
    private List<Toy> toys;

    public ToyRepositoryImpl() {
        toys = new ArrayList<>();
    }

    public List<Toy> getAllToys() {
        return toys;
    }

    public Toy getToyById(int id) {
        for (Toy toy : toys) {
            if (toy.getId() == id) {
                return toy;
            }
        }
        return null;
    }

    public List<Toy> searchToysByName(String name) {
        List<Toy> result = new ArrayList<>();
        for (Toy toy : toys) {
            if (toy.getName().toLowerCase().contains(name)) {
                result.add(toy);
            }
        }
        return result;
    }

    public List<Toy> searchToysByPrice(double minPrice, double maxPrice) {
        List<Toy> result = new ArrayList<>();
        for (Toy toy : toys) {
            if (toy.getPrice() >= minPrice && toy.getPrice() <= maxPrice) {
                result.add(toy);
            }
        }
        return result;
    }

    public void addToy(Toy toy) {
        // Check for duplicates
        if (toys.stream().anyMatch(t -> t.getName().equals(toy.getName()))) {
            throw new IllegalStateException("Toy with name " + toy.getName() + " already exists.");
        }

        toys.add(toy);
        saveToysToCsv("toys.csv"); // Persist changes to CSV
    }

    public void deleteToy(int toyId) {
        toys.removeIf(toy -> toy.getId() == toyId);
        saveToysToCsv("toys.csv"); // Persist changes to CSV
    }

    public List<String> getAllCategories() {
        List<String> categories = new ArrayList<>();
        for (Toy toy : toys) {
            if (!categories.contains(toy.getCategory())) {
                categories.add(toy.getCategory());
            }
        }
        return categories;
    }

    // Load toys from CSV file
    public void loadToysFromCsv(String filePath) {
        // Existing load method...
    }

    // Save toys to CSV file
    public void saveToysToCsv(String filePath) {
        // Existing save method...
    }
}
