package com.mazaitov.controller;

import com.mazaitov.model.Toy;
import com.mazaitov.service.ToyService;

import java.util.List;
import java.util.Iterator;

public class ToyController {
    private ToyService toyService;

    public ToyController(ToyService toyService) {
        this.toyService = toyService;
    }

    public void displayAllToys() {
        List<Toy> toys = toyService.getAllToys();
        printToys(toys);
    }

    public void searchToysByName(String name) {
        List<Toy> toys = toyService.searchToysByName(name);
        printToys(toys);
    }

    public void searchToysByPrice(double minPrice, double maxPrice) {
        List<Toy> toys = toyService.searchToysByPrice(minPrice, maxPrice);
        printToys(toys);
    }

    public boolean deleteToyById(int id) {
        List<Toy> toys = toyService.getAllToys();
        Iterator<Toy> iterator = toys.iterator();

        while (iterator.hasNext()) {
            Toy toy = iterator.next();
            if (toy.getId() == id) {
                iterator.remove();
                return true; // Deletion successful
            }
        }

        return false; // Toy with specified ID not found
    }

    private void printToys(List<Toy> toys) {
        if (toys.isEmpty()) {
            System.out.println("No toys found.");
            return;
        }

        System.out.println("Toy Warehouse - All Toys:");
        System.out.println("------------------------------------------");
        for (Toy toy : toys) {
            System.out.println("ID: " + toy.getId() +
                    ", Name: " + toy.getName() +
                    ", Category: " + toy.getCategory() +
                    ", Price: " + toy.getPrice());
        }
        System.out.println("------------------------------------------");
    }
}
