
package com.mazaitov.controller;

import com.mazaitov.model.Toy;
import com.mazaitov.service.ToyService;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ToyControllerTest {

    @Test
    public void testDisplayAllToys() {
        ToyService toyService = mock(ToyService.class);
        ToyController toyController = new ToyController(toyService);

        // Add test logic for displayAllToys method
        List<Toy> mockToys = createMockToys(); // You need to implement createMockToys method
        when(toyService.getAllToys()).thenReturn(mockToys);

        // Example assertion, modify based on your actual implementation
        assertEquals(mockToys.size(), toyController.displayAllToys());
    }

    // Add more test methods for other methods in ToyController

    private List<Toy> createMockToys() {
        // Implement this method to create a list of mock toys for testing
        List<Toy> mockToys = new ArrayList<>();
        // Add mock toys to the list
        return mockToys;
    }
}
