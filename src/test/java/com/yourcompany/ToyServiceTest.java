import com.mazaitov.model.Toy;
import com.mazaitov.repository.ToyRepository;
import com.mazaitov.service.ToyService;
import com.mazaitov.service.ToyServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ToyServiceTest {
    private ToyRepository toyRepository;
    private ToyService toyService;

    @Before
    public void setUp() {
        // Create a mock ToyRepository for testing
        toyRepository = mock(ToyRepository.class);
        toyService = new ToyServiceImpl(toyRepository);
    }

    @Test
    public void testGetAllToys() {
        // Mock data for testing
        List<Toy> mockToys = Arrays.asList(
                new Toy(1, "Toy1", "Category1", 10.0),
                new Toy(2, "Toy2", "Category2", 20.0)
        );

        // Mock the behavior of the toyRepository
        when(toyRepository.getAllToys()).thenReturn(mockToys);

        // Test the getAllToys method
        List<Toy> result = toyService.getAllToys();

        // Verify the result
        assertEquals(mockToys, result);
    }

    // Similar tests for other methods...

    // You can add more tests for other methods in ToyService
}
