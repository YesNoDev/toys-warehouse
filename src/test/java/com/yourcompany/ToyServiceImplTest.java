import com.mazaitov.model.Toy;
import com.mazaitov.repository.ToyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ToyServiceImplTest {
    private ToyServiceImpl toyService;
    private ToyRepository toyRepository;

    @BeforeEach
    void setUp() {
        toyRepository = mock(ToyRepository.class);
        toyService = new ToyServiceImpl(toyRepository);
    }

    @Test
    void getAllToys_NoToysInRepository_ReturnsEmptyList() {
        when(toyRepository.getAllToys()).thenReturn(new ArrayList<>());

        List<Toy> toys = toyService.getAllToys();

        assertTrue(toys.isEmpty());
    }

    @Test
    void getAllToys_ToysInRepository_ReturnsListOfToys() {
        List<Toy> expectedToys = new ArrayList<>();
        expectedToys.add(new Toy(1, "Teddy Bear", "Stuffed Animals", 19.99));
        expectedToys.add(new Toy(2, "Lego Set", "Building Blocks", 39.99));

        when(toyRepository.getAllToys()).thenReturn(expectedToys);

        List<Toy> actualToys = toyService.getAllToys();

        assertEquals(expectedToys, actualToys);
    }

    // Add more test methods for other scenarios

}
