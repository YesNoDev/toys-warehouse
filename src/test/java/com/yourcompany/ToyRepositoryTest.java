package com.mazaitov.repository;

import com.mazaitov.model.Toy;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ToyRepositoryTest {

    @Test
    public void testGetAllToys() {
        ToyRepository toyRepository = new ToyRepositoryImpl();

        // Add test logic for getAllToys method
        List<Toy> toys = toyRepository.getAllToys();

        // Example assertion, modify based on your actual implementation
        assertNotNull(toys);
        assertEquals(0, toys.size());
    }

    // Add more test methods for other methods in ToyRepository

}
