import com.mazaitov.model.Toy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ToyRepositoryImplTest {
    private ToyRepositoryImpl toyRepository;

    @BeforeEach
    void setUp() {
        toyRepository = new ToyRepositoryImpl();
    }

    @Test
    void getAllToys_NoToysInRepository_ReturnsEmptyList() {
        List<Toy> toys = toyRepository.getAllToys();

        assertTrue(toys.isEmpty());
    }

    @Test
    void getAllToys_ToysInRepository_ReturnsListOfToys() {
        List<Toy> expectedToys = new ArrayList<>();
        expectedToys.add(new Toy(1, "Teddy Bear", "Stuffed Animals", 19.99));
        expectedToys.add(new Toy(2, "Lego Set", "Building Blocks", 39.99));

        toyRepository.loadToysFromCsv("toys.csv");
        List<Toy> actualToys = toyRepository.getAllToys();

        assertEquals(expectedToys, actualToys);
    }

    // Add more test methods for other scenarios

}
