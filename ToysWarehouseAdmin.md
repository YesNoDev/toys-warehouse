ToysWarehouseAdmin Documentation
Overview
ToysWarehouseAdmin is a Toys Warehouse Search System designed to manage and display information about various toys available in a warehouse. The system provides a user-friendly interface to view all toys, search for toys by name, and filter toys based on price range. Additionally, it offers administrative features for authorized users to manage toy records.

Project Structure
The project follows a modular and organized structure to enhance readability, maintainability, and scalability. Key components include:

Main.java: The entry point of the application, responsible for initializing the system and handling user interactions.

ToyController.java: Manages user interactions, coordinates responses, and provides functionality to display, search, add, and delete toy records.

ToyService.java and ToyServiceImpl.java: Implements business logic related to toys, such as loading and saving toy data from/to a CSV file.

ToyRepository.java and ToyRepositoryImpl.java: Interacts with the data layer to retrieve, store, and manage toy data.

Toy.java: Defines the Toy model representing individual toy records.

Getting Started
Prerequisites
Java installed on your machine.
A Java IDE such as IntelliJ IDEA for development.
Installation
Clone the Git repository:

bash
Copy code
git clone https://gitlab.com/YesNoDev/toys-warehouse
Open the project in your Java IDE.

Run Main.java to start the Toy Warehouse Search System.

User Interface
The system provides a menu-driven interface with the following options:

Option 1: Display all toys.
Option 2: Search toys by name.
Option 3: Search toys by price range.
Option 4: To become an Administrator (requires authorization).
Option 0: Exit the application.
Administrator Options
If the user chooses to become an Administrator:

Enter a username and password for authorization.
Gain access to additional options:
Option 1: Add new records (elements).
Option 2: Delete records (elements).
Option 3: Add new types (categories).
Option 0: Exit, saving any changes made to the toy data.
Testing
The project includes built-in tests to verify correct operation and identify possible errors. Test logic is implemented for various methods in ToyRepository and ToyController. To run tests, use the testing features provided by your IDE or use a testing framework such as JUnit.

Contributing
Contributions to the project are welcome. If you encounter issues or have suggestions for improvements, feel free to open an issue or submit a pull request.

Author
Author: Jamshid 
Date: 08.01.2023
This document serves as an introductory guide for users and contributors. Customize it further to provide specific details about your project and its functionalities.





                                                                      |
